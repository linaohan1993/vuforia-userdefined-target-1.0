package com.qualcomm.vuforia.samples.SampleApplication.utils;

import java.nio.Buffer;

public class LineObject extends MeshObject
{
    // Data for drawing the 3D plane as overlay
	//直线的顶点数组
    private float[] LineVertices = new float[10000];
    public int[] breakPoints = new int[100];
    public int numOfVerts = 0;
    public int numOfShapes = 0;
    //直线的颜色数组
    private float LineColor[] = { 
    	//r,g,b,a
    		0.0f,1.0f,1.0f,0.5f
    };
    
    private Buffer mVertBuff;
    private Buffer mVertColor;
    
    public LineObject()
    {
        mVertBuff = fillBuffer(LineVertices);
        mVertColor = fillBuffer(getLineColor());
    }
    
    //设置直线的顶点，传入一个float型的顶点数组，转换成浮点型缓冲区
    public void setLineVerts(float[] lineVerts,int numOfvertices,int[] breakpoints,int numOfBP){
    	LineVertices = lineVerts;
    	breakPoints = breakpoints;
    	numOfVerts = numOfvertices;
    	numOfShapes = numOfBP;
    	mVertBuff = fillBuffer(LineVertices);
    }
    
    @Override
    public Buffer getBuffer(BUFFER_TYPE bufferType)
    {
        Buffer result = null;
        switch (bufferType)
        {
            case BUFFER_TYPE_VERTEX:
                result = mVertBuff;
                break;
            default:
                break;
        }
        return result;
    }
    
    
    @Override
    public int getNumObjectVertex()
    {
        return LineVertices.length / 3;
    }
    
    
    @Override
    public int getNumObjectIndex()
    {
        return LineVertices.length;
    }

	public float[] getLineColor() {
		return LineColor;
	}
	public void setLineColor(float lineColor[]) {
		LineColor = lineColor;
	}
}
