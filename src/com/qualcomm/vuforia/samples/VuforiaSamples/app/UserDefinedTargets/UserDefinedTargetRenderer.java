/*==============================================================================
 Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc.
 All Rights Reserved.
 ==============================================================================*/

package com.qualcomm.vuforia.samples.VuforiaSamples.app.UserDefinedTargets;

import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.R.integer;
import android.R.interpolator;
import android.R.string;
import android.opengl.GLES10;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.renderscript.Matrix2f;
import android.util.Log;

import com.qualcomm.vuforia.Matrix44F;
import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Tool;
import com.qualcomm.vuforia.Trackable;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.VIDEO_BACKGROUND_REFLECTION;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationSession;
import com.qualcomm.vuforia.samples.SampleApplication.utils.LineShaders;
import com.qualcomm.vuforia.samples.SampleApplication.utils.LinesVector;
import com.qualcomm.vuforia.samples.SampleApplication.utils.SampleUtils;
import com.qualcomm.vuforia.samples.SampleApplication.utils.LineObject;
import com.qualcomm.vuforia.samples.SampleApplication.utils.Texture;


// The renderer class for the ImageTargetsBuilder sample. 
public class UserDefinedTargetRenderer implements GLSurfaceView.Renderer
{
    private static final String LOGTAG = "UserDefinedTargetRenderer";
    
    SampleApplicationSession vuforiaAppSession;
    
    public boolean mIsActive = false;
    
    private Vector<Texture> mTextures;
    
    private int shaderProgramID; 							//程序要用到的shader容器
    private int Isfirstdraw=0;  //是否第一个object已经画过了
    private int vertexHandle;								//顶点索引
    
    private int mColorHandle;								//颜色索引
    
    private int mvpMatrixHandle;							//模型视图投影矩阵索引
    private int nowtrackable=0; //当前正在追踪的物体编号，用于控制是否需要实现追踪再画
    public LineObject mLine;								//要绘制的对象
   
    
    
    static final float kObjectScale = 8.f; 					//修改kObjectScale可以调整模型视图显示比例
    
    private UserDefinedTargets mActivity;					//主界面的引用
    
    
    public UserDefinedTargetRenderer(UserDefinedTargets activity,
        SampleApplicationSession session)
    {
        mActivity = activity;
        vuforiaAppSession = session;
    }
    
    
    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");
        
        // Call function to initialize rendering:
        initRendering();
        
        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();
    }
    
    
    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");
        
        // Call function to update rendering when render surface
        // parameters have changed:
        mActivity.updateRendering();
        
        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);
    }
    
    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl)
    {
        if (!mIsActive)
            return;
        
        // Call our function to render content
        renderFrame();
    }
    
    //画用户生成的内容
    public void drawLines(int trackableindex){
    	//（注意：这里要用GL_LINE_STRIP属性而不是GL_LINES）
    	//画第一个图形
    	Log.i("Isupdate",Integer.toString(LinesVector.Isupdate));
    	
    	if(LinesVector.manyobject>0){
    		if((LinesVector.manyobject==1&&LinesVector.Isupdate>0&&trackableindex==0)||(Isfirstdraw==1&&trackableindex==0)){
    			Isfirstdraw=1;
    			GLES10.glLineWidth(6.0f);
    			GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, mLine.breakPoints[0]);
    		    
    		     Log.i("first breakpoint ",String.valueOf(mLine.breakPoints[0]));
    			 Log.i("drawlines index ",LinesVector.trackablelinecount.get(0).toString());
    			 for(int i=0;i<Integer.valueOf(LinesVector.trackablelinecount.get(0).toString())-1;i++){
    				 Log.i("test2 ","i am here1");
    				 GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, mLine.breakPoints[i],mLine.breakPoints[i+1]-mLine.breakPoints[i]);
    		   }
    	}
    		else{
    		  if(LinesVector.Isupdate>0){
    			  for(int i=Integer.valueOf(LinesVector.trackablelinestart.get(trackableindex).toString())-Integer.valueOf(LinesVector.trackablelinecount.get(trackableindex).toString())-1;i<Integer.valueOf(LinesVector.trackablelinestart.get(trackableindex).toString())-1;i++){
    				  GLES10.glLineWidth(6.0f);
    				  Log.i("test2 ","i am here2");
    				  GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, mLine.breakPoints[i],mLine.breakPoints[i+1]-mLine.breakPoints[i]);
     		   }
    		  }
    		}
    	
    }
    }   
    private void renderFrame()
    {
        // Clear color and depth buffer
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        // Get the state from Vuforia and mark the beginning of a rendering
        // section
        State state = Renderer.getInstance().begin();
        
        // Explicitly render the Video Background
        Renderer.getInstance().drawVideoBackground();
        
        int trackableindex=0;
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        if (Renderer.getInstance().getVideoBackgroundConfig().getReflection() == VIDEO_BACKGROUND_REFLECTION.VIDEO_BACKGROUND_REFLECTION_ON)
            GLES20.glFrontFace(GLES20.GL_CW); // Front camera
        else
            GLES20.glFrontFace(GLES20.GL_CCW); // Back camera
            
        // Render the RefFree UI elements depending on the current state
        mActivity.refFreeFrame.render();
        Log.i("num trackbleResults: ", Integer.toString(state.getNumTrackableResults()));
        // Did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++)
        {
            // Get the trackable:
            TrackableResult trackableResult = state.getTrackableResult(tIdx);
            Trackable trackable11=trackableResult.getTrackable();
            String indexname;
            String indexname1;
            indexname=trackable11.getName();
            indexname1= indexname.split("-")[1];
            trackableindex=Integer.valueOf(indexname1).intValue();
            Log.i("TRACK NAME Index ",indexname1);
            
//            if(nowtrackable!=trackableindex){
//            	LinesVector.Ismakeobject=0;
//            	nowtrackable=trackableindex;
//            	 }
            
            Log.i("is checkable changed",Integer.toString(LinesVector.Ismakeobject));
            //这是SDK能做的事，将模型矩阵转换为OPENGL可以用的矩阵
            Matrix44F modelViewMatrix_Vuforia = Tool
                .convertPose2GLMatrix(trackableResult.getPose());
            //从Vuforia SDK中获取模型视图矩阵
            float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();
            
            //定义模型视图投影矩阵
            float[] modelViewProjection = new float[16];
            
            //设置模型视图矩阵的平移值（0,0,3）
            Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, kObjectScale);
            Matrix.rotateM(modelViewMatrix, 0, -90, 0, 0, 1);
            Matrix.rotateM(modelViewMatrix, 0, 180, 0, 1, 0);
            //设置模型视图矩阵的缩放值（3,3,3）
            Matrix.scaleM(modelViewMatrix, 0, kObjectScale+.4f, kObjectScale,
                kObjectScale);
            
            //将投影矩阵与模型视图矩阵相乘，得到模型视图投影矩阵
            Matrix.multiplyMM(modelViewProjection, 0, vuforiaAppSession
                .getProjectionMatrix().getData(), 0, modelViewMatrix, 0);
            
            //GLSL语言，program包含一个或多个形状的shader，这里将program添加到环境中
            GLES20.glUseProgram(shaderProgramID);

            //设置模型视图投影矩阵
            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
                modelViewProjection, 0);
            
            //设置缓冲区起始位置
            mLine.getVertices().position(0);
            
            //顶点位置数据传入着色器
            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
                    false, 3*4, mLine.getVertices());
            
            //设置直线的颜色
            GLES20.glUniform3fv(mColorHandle, 1, mLine.getLineColor(),0);
            
            GLES20.glEnableVertexAttribArray(vertexHandle);
            GLES20.glEnableVertexAttribArray(mColorHandle);
            
            //画线
            drawLines(trackableindex);
            
            GLES20.glDisableVertexAttribArray(vertexHandle);
            GLES20.glDisableVertexAttribArray(mColorHandle);
            
            SampleUtils.checkGLError("UserDefinedTargets renderFrame");
        }
        
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        
        Renderer.getInstance().end();
    }
    
    
    private void initRendering()
    {
        Log.d(LOGTAG, "initRendering");
        //创建要绘制的对象，完成对象的初始化
        mLine = new LineObject();
        
        // Define clear color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
            : 1.0f);
        
        // Now generate the OpenGL texture objects and add settings
        for (Texture t : mTextures)
        {
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
                GLES20.GL_UNSIGNED_BYTE, t.mData);
        }
        
        shaderProgramID = SampleUtils.createProgramFromShaderSrc(
                LineShaders.LINE_VERTEX_SHADER,
                LineShaders.LINE_FRAGMENT_SHADER);
        
        //获取指向Vertex Shader的成员VertexPosition的handle
        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
            "vertexPosition");
        
        //获取指向fragment shader的成员color的handle
        mColorHandle = GLES20.glGetUniformLocation(shaderProgramID,"color");
        
        //获取指向fragment shader成员MVPMatrix的handle
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
            "modelViewProjectionMatrix");
    }
    
    
    public void setTextures(Vector<Texture> textures)
    {
        mTextures = textures;
        
    }
    
}
